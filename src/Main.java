/**
 * Created by amen on 8/14/17.
 */
public class Main {
    public static void main(String[] args) {
        Hero newHero = new Hero("Zenek");
        System.out.println(newHero);

        HeroOnAHorse decorated = new HeroOnAHorse(newHero);
        System.out.println(decorated);

        System.out.println(newHero.getAttackPoints());
        System.out.println(decorated.getAttackPoints());
    }
}
